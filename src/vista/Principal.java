package vista;

import javax.swing.JButton;

public class Principal extends javax.swing.JFrame {


    public Principal() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jb_proveedores = new javax.swing.JButton();
        jb_cliente = new javax.swing.JButton();
        jb_compras = new javax.swing.JButton();
        jb_productos = new javax.swing.JButton();
        jb_ventas = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jb_proveedores.setText("Proveedores");

        jb_cliente.setText("Cliente");

        jb_compras.setText("Compras");

        jb_productos.setText("Productos");

        jb_ventas.setText("Ventas");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jb_productos)
                    .addComponent(jb_proveedores))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
                        .addComponent(jb_ventas))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jb_cliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jb_compras)))
                .addGap(28, 28, 28))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jb_proveedores)
                    .addComponent(jb_cliente)
                    .addComponent(jb_compras))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addComponent(jb_productos))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(jb_ventas)))
                .addContainerGap(99, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //Geters y Seters de cada uno de los botones que tendran una accion
    public JButton getJb_cliente() {
        return jb_cliente;
    }

    public JButton getJb_compras() {
        return jb_compras;
    }

    public JButton getJb_productos() {
        return jb_productos;
    }

    public JButton getJb_proveedores() {
        return jb_proveedores;
    }

    public JButton getJb_ventas() {
        return jb_ventas;
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jb_cliente;
    private javax.swing.JButton jb_compras;
    private javax.swing.JButton jb_productos;
    private javax.swing.JButton jb_proveedores;
    private javax.swing.JButton jb_ventas;
    // End of variables declaration//GEN-END:variables
}
