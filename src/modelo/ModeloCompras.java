package modelo;

public class ModeloCompras {
    
    //Declaracion de variables que tendra la clase compras
    private String id_cliente;
    private String id_producto;
    private double costo;

    public ModeloCompras() {
    }
    
    //Constructor que inicializa las variables de clase
    public ModeloCompras(String id_cliente, String id_producto, double costo) {
        this.id_cliente = id_cliente;
        this.id_producto = id_producto;
        this.costo = costo;
    }

    //Geters y Seters de las variables de clase
    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getId_producto() {
        return id_producto;
    }

    public void setId_producto(String id_producto) {
        this.id_producto = id_producto;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
    
}
