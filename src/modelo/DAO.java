package modelo;

import Singleton.Singleton;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class DAO<T> {
  
//Declaracion de parametros que se usaran para la utilizacion de la base de datos
  protected int indice;
  protected PreparedStatement ps;
  protected ResultSet rs;
  protected Singleton conexion;
  
  //Constructor que inicializara la clase Singleton
  public DAO(Singleton conexion){
    this.conexion = conexion;
  }
  
  //Declaracion de metodos abstractos para poder imlementar en las clases DAO
  public abstract void add(T obj);
  public abstract void edit(T obj);
  public abstract void remove(T obj);
  public abstract ResultSet consultar(String storedProcedure);
}
