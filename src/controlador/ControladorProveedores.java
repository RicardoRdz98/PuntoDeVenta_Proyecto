package controlador;

import Singleton.Singleton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.ModeloProveedores;
import modelo.ProveedoresDAO;
import vista.Proveedores;

/**
 *
 * @author rodri
 */
public class ControladorProveedores implements ActionListener{

    private Proveedores vista;
    private ModeloProveedores modelo;

    public ControladorProveedores() {
    }

    public ControladorProveedores(Proveedores vista, ModeloProveedores modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    
    public void iniciar(){
        vista.setTitle("Vista Proveedores");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }
    
    private void asignarControl() {
        vista.getJb_guardar().addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == vista.getJb_guardar()){
            
            if(vista.getTf_direccion().getText().equals("") || vista.getTf_nombre().getText().equals("")
            || vista.getTf_telefono().getText().equals(""))
            {
                JOptionPane.showMessageDialog(null, "Algun campo se encuentra vacio, favorde completarlos");
            }
            else
            {
                try {
                    ProveedoresDAO p = new ProveedoresDAO(Singleton.crearConexion());
                    modelo.setNombre(vista.getTf_nombre().getText());
                    modelo.setDireccion(vista.getTf_direccion().getText());
                    modelo.setTelefono(vista.getTf_telefono().getText());
                    vista.getTf_telefono().setText("");
                    vista.getTf_direccion().setText("");
                    vista.getTf_nombre().setText("");
                    p.add(modelo);
                    JOptionPane.showMessageDialog(null, "Datos almacenados");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ControladorProveedores.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorProveedores.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
