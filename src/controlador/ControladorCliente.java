package controlador;

import Singleton.Singleton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.ClientesDAO;
import modelo.ModeloCliente;
import vista.Clientes;

public class ControladorCliente implements ActionListener{

    private Clientes vista;
    private ModeloCliente modelo;

    public ControladorCliente(Clientes vista, ModeloCliente modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    
    public void iniciar(){
        vista.setTitle("Vista Clientes");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }
    
    private void asignarControl(){
        vista.getJb_guardar().addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == vista.getJb_guardar()){
            
            //Exepcion para no mandar datos vacios a la base de datos
            if(vista.getTf_nombres().getText().equals("") || vista.getTf_apellidos().getText().equals("")
               ||vista.getTf_direccion().getText().equals("") || vista.getTf_telefono().getText().equals("")){
                JOptionPane.showMessageDialog(null, "Algun campo se encuentra vacio, favor de completarlos");
            }else{
                try {
                //Inicialisacion de la clase ClientesDAO con la instancia del patron de diseño Singleton
                ClientesDAO p = new ClientesDAO(Singleton.crearConexion());
                //Asignacion al modelo Cliente de cada uno de los datos traidos de la vista cliente 
                modelo.setNombre(vista.getTf_nombres().getText());
                modelo.setApellido(vista.getTf_apellidos().getText());
                modelo.setDireccion(vista.getTf_direccion().getText());
                modelo.setTelefono(vista.getTf_telefono().getText());
                //Mandando la clase modelo para la clase ClientesDAO para realizar el insert de los datos en la base de datos
                p.add(modelo);
                JOptionPane.showMessageDialog(null, "Datos almacenados");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ControladorProveedores.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorProveedores.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }
    }
    
}
