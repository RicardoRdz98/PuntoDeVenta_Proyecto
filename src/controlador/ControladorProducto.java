package controlador;

import Singleton.Singleton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.ModeloProducto;
import modelo.ProductoDAO;
import vista.Productos;

public class ControladorProducto implements ActionListener {

    private Productos vista;
    private ModeloProducto modelo;

    public ControladorProducto(Productos vista, ModeloProducto modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    
    public void iniciar(){
        vista.setTitle("Vista Productos");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }
    
    private void asignarControl(){
        vista.getTf_nombre().addActionListener(this);
        vista.getTf_precio().addActionListener(this);
        vista.getJb_guardar().addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == vista.getJb_guardar()){
            
            //Exepcion para no mandar datos vacios a la base de datos
            if(vista.getTf_nombre().getText().equals("") || vista.getTf_precio().getText().equals("")){
                JOptionPane.showMessageDialog(null, "Algun campo se encuentra vacio, favor de completarlos");
            }else{
                try {
                //Inicialisacion de la clase ClientesDAO con la instancia del patron de diseño Singleton
                ProductoDAO p = new ProductoDAO(Singleton.crearConexion());
                //Asignacion al modelo Cliente de cada uno de los datos traidos de la vista cliente 
                modelo.setNombre(vista.getTf_nombre().getText());
                double precio = Double.parseDouble(vista.getTf_precio().getText());
                modelo.setPrecio(precio);
                //Mandando la clase modelo para la clase ClientesDAO para realizar el insert de los datos en la base de datos
                p.add(modelo);
                JOptionPane.showMessageDialog(null, "Datos almacenados");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ControladorProveedores.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(ControladorProveedores.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }
        
    }
    
}
