package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.ModeloCompras;
import vista.Compras;

public class ControladorCompras implements ActionListener{

    private Compras vista;
    private ModeloCompras modelo;

    public ControladorCompras(Compras vista, ModeloCompras modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    
    public void iniciar(){
        vista.setTitle("Vista Compras");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }
    
    private void asignarControl(){
        vista.getJcb_clientes().addActionListener(this);
        vista.getJcb_productos().addActionListener(this);
        vista.getJb_realizar().addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
    
}
